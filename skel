#!/bin/sh

# skel

print_usage() {
    echo "Usage: $0 PROJECT_NAME"
}

# safe shell
set -e -u -f

# safe stuff (warn if root)
EUID=`id -u`
if [ "$EUID" -eq 0 ]; then
    echo "WARNING: It is not recommended to run this as the root user"
fi

# parse params (all we need is a name)
if [ "$#" -ne 1 ]; then
    print_usage
    exit 1
fi

# Check if (somehow) the argument is unset or is blank
if [ -z "$1" ]; then
    print_usage
    exit 1
fi

# A directory needs to be created with the same name as the project
PROJECT_NAME="$1"
SOURCE_FILE_NAME="$1.cpp"

# Try to extract the user details from git configurarion if it exists
AUTHOR_NAME='[AUTHOR NAME]'
AUTHOR_EMAIL='[AUTHOR EMAIL]'
if command -v git > /dev/null 2>&1
then
    AUTHOR_NAME=`git config user.name`
    AUTHOR_EMAIL=`git config user.email`
fi

# Create the project directory
mkdir ./"$PROJECT_NAME" && \
    cd ./"$PROJECT_NAME"

if [ "$?" -ne 0 ]; then
    echo "ERROR: Failed to initialize project in the current directory."
    exit 1
fi

# Create test file (which will be blank, initially)
touch input.txt

# Create source file and populate it with a template
printf "%s\n" \
    '/*'                              \
    " * $PROJECT_NAME"                \
    ' *'                              \
    " * $AUTHOR_NAME"                 \
    ' *'                              \
    " * Created: $(date)"             \
    ' *'                              \
    ' */'                             \
    ''                                \
    '#include <cstdio>'               \
    ''                                \
    'int main() {'                    \
    ''                                \
    '    return 0;'                   \
    '}'                               \
    ''                                \
> $SOURCE_FILE_NAME

# Do the same for the Makefile
printf "%s\n" \
    "SOURCE_FILE_NAME=$SOURCE_FILE_NAME"                       \
    "EXEC_FILE_NAME=$PROJECT_NAME"                             \
    ''                                                         \
    'all: build'                                               \
    'install: build'                                           \
    ''                                                         \
    'build: clean'                                             \
    '	mkdir -p ./build'                                      \
    '	g++ ${SOURCE_FILE_NAME} -o ./build/${EXEC_FILE_NAME}'  \
    ''                                                         \
    'run: build'                                               \
    '	./build/${EXEC_FILE_NAME} < input.txt'                 \
    ''                                                         \
    'clean:'                                                   \
    '	rm -rf ./build'                                         \
    ''                                                         \
> Makefile


echo "Initialized project $PROJECT_NAME in currect directory."
exit 0

